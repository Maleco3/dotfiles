set tabstop=4
set softtabstop=4
set shiftwidth=4
set textwidth=79
set expandtab
set autoindent
set fileformat=unix
set cursorline

" Linebreak on 120 characters
set lbr
set tw=80

set encoding=utf-8

let python_highlight_all=1
syntax on
