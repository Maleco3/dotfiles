#!/bin/bash
########## Variables

dir=~/dotfiles                    # dotfiles directory
olddir=~/dotfiles_old             # old dotfiles backup directory
files="viminfo vimrc vim uncrustify.cfg atom zlogin zlogout zpreztorc zprofile zshenv zshrc"        # list of files/folders to symlink in homedir
##########

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

for file in $files; do
# for file in $DIRFILES; do
  f=$(basename $file)
  echo "processing $f"
  echo "Moving any existing dotfiles from ~ to $olddir"
  mv ~/.$f ~/dotfiles_old/
  echo "Creating symlink to $file in home directory."
  ln -s $dir/$f ~/.$f
done

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks

#source ~/.vimrc
