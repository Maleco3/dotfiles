" ---------------------- USABILITY CONFIGURATION ----------------------
"  Basic and pretty much needed settings to provide a solid base for
"  source code editting
" set the color theme to wombat256
colorscheme vividchalk
"
" don't make vim compatible with vi 
set nocompatible

" turn on syntax highlighting
syntax on
" and show line numbers
set number
" Disable system bell 
set vb t_vb=.
" Nice tab names
set guitablabel=%t
" reload files changed outside vim
set autoread         
" encoding is utf 8
set encoding=utf-8
set fileencoding=utf-8

" enable matchit plugin which ships with vim and greatly enhances '%'
runtime macros/matchit.vim

" by default, in insert mode backspace won't delete over line breaks, or 
" automatically-inserted indentation, let's change that
set backspace=indent,eol,start

" dont't unload buffers when they are abandoned, instead stay in the
" background
set hidden

" set unix line endings
set fileformat=unix
" when reading files try unix line endings then dos, also use unix for new
" buffers
set fileformats=unix,dos

" save up to 100 marks, enable capital marks
set viminfo='100,f1

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

"Always show current position
set ruler

" Height of the command bar
set cmdheight=1

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch

" ---------------------- CUSTOMIZATION ----------------------
"  The following are some extra mappings/configs to enhance my personal
"  VIM experience

" set , as mapleader
let mapleader = ","

" map <leader>q and <leader>w to buffer prev/next buffer
noremap <leader>q :bp<CR>
noremap <leader>w :bn<CR>

" windows like clipboard
" yank to and paste from the clipboard without prepending "* to commands
let &clipboard = has('unnamedplus') ? 'unnamedplus' : 'unnamed'

" hide unnecessary gui in gVim
if has("gui_running")
    set guioptions-=m  " remove menu bar
    set guioptions-=T  " remove toolbar
    set guioptions-=r  " remove right-hand scroll bar
    set guioptions-=L  " remove left-hand scroll bar
end

" Be smart when using tabs ;)
set smarttab

" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2
set expandtab


" Linebreak on 120 characters
set lbr
set tw=120

" Show the column end
set colorcolumn=121
highlight ColorColumn ctermbg=darkgray

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" Show line numbers 
set number

" Show indent guides "
set listchars=tab:\|\ 
set list 

" Set the backup/temp folder "
set backupdir =/tmp/
set directory =/tmp/

" Set the foldmethod"
autocmd FileType c setlocal foldmethod=syntax

filetype plugin indent on
"
" start NERDTree on start-up and focus active window 
autocmd VimEnter * NERDTree
autocmd BufEnter * NERDTreeMirror
autocmd VimEnter * wincmd p

" show any linting errors immediately
let g:syntastic_check_on_open = 1

if has('gui running')
    set guifont=Droid\ Sans\ Mono\ for\ Powerline\ Nerd\ Font\ 10
endif

" Nice indentguides
let g:indentLine_char = '|'

" Omnicomplete (autocomplete)
set omnifunc=syntaxcomplete#Complete
